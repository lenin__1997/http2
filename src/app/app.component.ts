import { Component } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name: string
  result:string
  constructor(private http:HttpClient){
    this.name={}as string
    this.result={}as string
  }
  postData(){
    let url = "http://localhost:5555/products"
    this.http.post(url,{
      name:this.name
    }).toPromise().then((data:any)=>{
      console.log(data)
      console.log(JSON.stringify(data.json.name))
      this.result = JSON.stringify(data.json.name)
    })

  }

}
